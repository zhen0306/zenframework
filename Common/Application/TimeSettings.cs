using System;

namespace Common
{
  public static class TimeSettings
  {
    public const long FixedUpdateRate = 16;
    public const long NetworkUpdateRate = 100;
  }
}

