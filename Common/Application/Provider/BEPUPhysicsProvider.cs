using System;
using Ninject.Activation;
using BEPUphysics;

namespace Common
{
  public class BEPUPhysicsProvider : Provider<Space>
  {
    #region implemented abstract members of Provider
    protected override Space CreateInstance (IContext context)
    {
      var physicsSpace = new BEPUphysics.Space();
      physicsSpace.ForceUpdater.Gravity = new BEPUutilities.Vector3(0, -16f, 0);
      physicsSpace.BufferedStates.Enabled = true;
      physicsSpace.Solver.IterationLimit = 5; //Don't need many iterations, there's not really any stacking going on in this game.
      physicsSpace.TimeStepSettings.TimeStepDuration = 1/60.0f; //A slower timestep gives the Xbox a little breathing 

      return physicsSpace;
    }
    #endregion
  }
}

