using System;
using BEPUphysics;

namespace Common.Applications.Modules
{
  public class PhysicsInstaller : Ninject.Modules.NinjectModule
  {
    #region implemented abstract members of NinjectModule
    public override void Load ()
    {
      Bind<Space> ().ToProvider<BEPUPhysicsProvider> ();
    }
    #endregion
  }
}

