﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Messaging
{
  public delegate void MessageBusHandler<T> (T message) where T : EventArgs;
  public interface IMessageBusChannel
  {
    void ProcessMessages ();

    void AddEventHandler<J> (MessageBusHandler<MessageBus<J>> eventHandler) where J : EventArgs;

    void AddMessage<J> (MessageBus<J> message) where J : EventArgs;

    void SendMessage<J> (MessageBus<J> message) where J : EventArgs;

    void RemoveEventHandler<J> (MessageBusHandler<MessageBus<J>> eventHandler) where J : EventArgs;
  }
}

