﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Messaging
{
  public delegate void Request ();
  public delegate void RequestHandler<T> (T args, Request request) where T : EventArgs;


  public class MessageBusManager
  {
    private Dictionary<Type, IMessageBusChannel> messageBusChannels = new Dictionary<Type, IMessageBusChannel> ();
    private List<MessageBusManager> subMessageBusManagers = new List<MessageBusManager> ();

    public void AddSubMessageBusManager (MessageBusManager messageBusManager)
    {
      lock (subMessageBusManagers)
      {
        subMessageBusManagers.Add (messageBusManager);
      }
    }

    public void RemoveSubMessageBusManager (MessageBusManager messageBusManager)
    {
      lock (subMessageBusManagers)
      {
        subMessageBusManagers.Remove (messageBusManager);
      }
    }

    public void AddChannel<T> () where T : EventArgs
    {
      MessageBusChannel<T> channel = new MessageBusChannel<T> ();
      lock (messageBusChannels)
      {
        messageBusChannels.Add (typeof(T), channel);
      }
    }

    public void AddEventHandler<T> (MessageBusHandler<MessageBus<T>> eventHandler) where T : EventArgs
    {
      lock (messageBusChannels)
      {
        if (!messageBusChannels.ContainsKey (typeof(T)))
        {
          AddChannel<T> ();
        }
        messageBusChannels [typeof(T)].AddEventHandler<T> (eventHandler);
      }
    }

    public void RemoveEventHandler<T> (MessageBusHandler<MessageBus<T>> eventHandler) where T : EventArgs
    {
      lock (messageBusChannels)
      {
        if (messageBusChannels.ContainsKey (typeof(T)))
        {
          messageBusChannels [typeof(T)].RemoveEventHandler<T> (eventHandler);
        }
      }
    }

    public void AddMessage<T> (T message) where T : EventArgs
    {
      AddMessage<T> (new MessageBus<T> (message));
    }

    public void AddMessage<T> (MessageBus<T> message) where T : EventArgs
    {
      lock (messageBusChannels)
      {
        if (!messageBusChannels.ContainsKey (typeof(T)))
        {
          AddChannel<T> ();
        }

        messageBusChannels [typeof(T)].AddMessage<T> (message);
      }
    }

    public void SendMessage<T> (T message) where T : EventArgs
    {
      SendMessage<T> (new MessageBus<T> (message));
    }

    public void SendMessage<T> (MessageBus<T> message) where T : EventArgs
    {
      for (int i = 0; i < subMessageBusManagers.Count; i++)
      {
        subMessageBusManagers [i].SendMessage<T> (message);
      }

      if (!messageBusChannels.ContainsKey (typeof(T)))
      {
        return;
      }
      messageBusChannels [typeof(T)].SendMessage<T> (message);
    }

    public void ProcessMessages ()
    {
      foreach (IMessageBusChannel messageBusChannel in messageBusChannels.Values)
      {
        messageBusChannel.ProcessMessages ();
      }
    }
  }
}

