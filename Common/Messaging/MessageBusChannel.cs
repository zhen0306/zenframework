﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Messaging
{
  public class MessageBusChannel<T> : IMessageBusChannel where T : EventArgs
  {
    public List<MessageBusHandler<MessageBus<T>>> eventHandlers = new List<MessageBusHandler<MessageBus<T>>> ();
    public List<MessageBus<T>> queuedMessages = new List<MessageBus<T>> ();

    public void ProcessMessages ()
    {
      for (int i = 0; i < queuedMessages.Count; i++)
      {
        for (int j = 0; j < eventHandlers.Count; j++)
        {
          if (!queuedMessages [i].isConsumed)
            eventHandlers [j] (queuedMessages [i]);
        }
      }
      queuedMessages.Clear ();
    }

    public void AddEventHandler<J> (MessageBusHandler<MessageBus<J>> eventHandler) where J : EventArgs
    {
      eventHandlers.Add (eventHandler as MessageBusHandler<MessageBus<T>>);
    }

    public void RemoveEventHandler<J> (MessageBusHandler<MessageBus<J>> eventHandler) where J : EventArgs
    {
      eventHandlers.Remove (eventHandler as MessageBusHandler<MessageBus<T>>);
    }

    public void AddMessage<J> (MessageBus<J> message) where J : EventArgs
    {
      queuedMessages.Add (message as MessageBus<T>);
    }

    public void SendMessage<J> (MessageBus<J> message) where J : EventArgs
    {
      for (int j = 0; j < eventHandlers.Count; j++)
      {
        if (!message.isConsumed)
          eventHandlers [j] (message as MessageBus<T>);
      }
    }
  }
}
