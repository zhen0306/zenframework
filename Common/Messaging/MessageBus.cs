﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Messaging
{
    public class MessageBus<T> : EventArgs where T : EventArgs
    {
        public T data;
        public bool isConsumed = false;

        public MessageBus(T data)
        {
            this.data = data;
        }
    }
}
