using System;
using BEPUphysics;
using Common.Game;

namespace Common.GameEvents
{
  public class AddPhysicsObject :EventArgs
  {
    public GameEntity GameEntity { get; private set; }

    public ISpaceObject SpaceObject { get; private set; }

    public AddPhysicsObject (GameEntity gameEntity, ISpaceObject spaceObject)
    {
      GameEntity = gameEntity;
      SpaceObject = spaceObject;
    }
  }

  public class DestroyPhysicsObject: EventArgs
  {
    public GameEntity GameEntity { get; private set; }

    public ISpaceObject SpaceObject { get; private set; }

    public DestroyPhysicsObject (GameEntity gameEntity,
                                ISpaceObject spaceObject)
    {
      GameEntity = gameEntity;
      SpaceObject = spaceObject;
    }
  }

  public class InputEvent: EventArgs
  {
    public int Ack { get; private set; }
    public long ClientId { get; private set; }
    public InputTypes Input { get; private set; }

    public float DeltaTime { get; private set; }

    public InputEvent (
      InputTypes input,
      float deltaTime) : this(0, 0, input, deltaTime)
    {
    }

    public InputEvent (
      int ack,
      long clientId,
      InputTypes input,
      float deltaTime)
    {
      Ack = ack;
      ClientId = clientId;
      Input = input;
      DeltaTime = deltaTime;
    }
  }

  public class AckEvent : EventArgs
  {
    public int SnapshotId { get; private set; }

    public AckEvent(int id)
    {
      SnapshotId = id;
    }
  }
}

