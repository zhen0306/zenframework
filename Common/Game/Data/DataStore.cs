using System;
using System.Collections.Generic;
using DataLoader.Data;
using Common.Log;
using System.Linq;

namespace Common.DataLoader
{
  public class DataStore
  {
    private Dictionary<string, BaseData> dataMapping = new Dictionary<string, BaseData> ();
    private ILog log;

    public DataStore (ILog log)
    {
      this.log = log;
    }

    public T CreateData<T>() where T:BaseData
    {
      T data = Activator.CreateInstance<T> ();
      data.GUID = Guid.NewGuid ().ToString();
      AddData (data);
      return data;
    }

    public void AddData (BaseData data)
    {
      if (!dataMapping.ContainsKey (data.GUID))
      {
        dataMapping.Add (data.GUID, data);
      } else
      {
        log.Error ("Key already exists: " + data.GUID);
      }
    }

    public void RemoveData (string key)
    {
      if (dataMapping.ContainsKey (key))
        dataMapping.Remove (key);
      else
        log.Error ("Key does not exist: " + key);
    }

    public BaseData GetData (string key)
    {
      if (dataMapping.ContainsKey (key))
        return dataMapping [key];
      else
      {
        log.Error ("Key does not exist: " + key);
        return null;
      }
    }

    public T GetData<T>( string key ) where T:BaseData
    {
      BaseData data = GetData (key);

      return data != null ? (T)data : null;
    }

    public List<BaseData> GetAllData()
    {
      return dataMapping.Values.ToList ();
    }

    public List<T> GetAllDataOfType<T>() where T:BaseData
    {
      List<T> temp = new List<T> ();
      temp = dataMapping.Values.Where (data => data.GetType () == typeof(T)).Cast<T>().ToList();
      return temp;
    }
  }
}

