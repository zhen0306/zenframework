﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLoader.Data
{
  public class BaseData
  {
    public string GUID;
    public int ID;
    public string Name;
    public int Version;
  }
}
