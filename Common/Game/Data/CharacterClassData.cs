﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLoader.Data;

namespace Common.Game.Data
{
    public class CharacterClassData : BaseData
    {
        public string name;
        public List<int> abilities;
        public List<int> weapons;
        public List<int> skins;
    }
}
