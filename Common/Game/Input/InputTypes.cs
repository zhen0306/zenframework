using System;

namespace Common
{
  [Flags]
  public enum InputTypes
  {
    None = 0,
    Forward = 1,
    Back = 2,
    Left = 4,
    Right = 8,
    PrimaryFire = 16,
    SecondaryFire = 32,
    Jump = 64,
    ScrollUp = 128,
    ScrollDown = 256,
  }

  // extension method to make input testing easy!
  public static class InputTypeExtensions
  {
    public static bool ContainsFlag(this InputTypes flagSource, InputTypes testFlag)
    {
      return ((flagSource & testFlag) == testFlag);
    }
  }
}

