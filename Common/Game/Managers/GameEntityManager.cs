using System;
using Common.Game;
using System.Collections.Generic;

namespace Common.Game.Managers
{
  public class GameEntityManager
  {
    public Dictionary<int, GameEntity> Entities { get; private set; }

    public GameEntityManager ()
    {
      Entities = new Dictionary<int, GameEntity> ();
    }

    public void AddEntity(int id, GameEntity entity)
    {
      Entities.Add (id, entity);
    }

    public void RemoveEntity(int id)
    {
      Entities.Remove (id);
    }
  }
}

