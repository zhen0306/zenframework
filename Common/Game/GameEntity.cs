﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Messaging;
using Ninject;

namespace Common.Game
{
  public abstract class GameEntity : ICloneable
  {
    public abstract EntityTypes Type { get; }
    public Dictionary<Type, IComponent> Components { get; private set; }


    [Inject]
    public MessageBusManager MessageBusManager { get; private set; }

    public GameEntity ()
    {
      Components = new Dictionary<Type, IComponent> ();
    }

    public bool ContainsComponent<T>() where T: IComponent
    {
      return Components.ContainsKey (typeof(T));
    }

    public T GetComponent<T> () where T: IComponent
    {
      return (T)GetComponent (typeof(T));
    }

    public IComponent GetComponent (Type type)
    {

      if (Components.ContainsKey (type))
        return Components [type];
      else
      {
        //iterate through all types and see if they are assignable from type
        Type t = Components.Keys.FirstOrDefault ((T) => type.IsAssignableFrom (T));
        if (t != null)
        {
          //cache this result for performance
          Components.Add (type, Components [t]);
          return Components [t];
        }
        else
          return null;
      }
    }

    public T AddComponent<T> (T component) where T: IComponent
    {
      Type type = typeof(T);
      if (Components.ContainsKey (type))
        throw new DuplicateWaitObjectException ();

      Components.Add (type, component);
      return (T)Components [type];
    }

    public void RemoveComponent<T> () where T: IComponent
    {
      RemoveComponent (typeof(T));
    }

    public void RemoveComponent (Type type)
    {
      if (Components.ContainsKey (type))
        Components.Remove(type);
      else
        throw new KeyNotFoundException ();
    }

    #region ICloneable implementation

    public object Clone ()
    {
      return MemberwiseClone ();
    }

    #endregion
  }
}
