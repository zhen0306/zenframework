namespace Common.Game.Networking.Messages
{
  public enum MessageTypes : byte
  {
    Connect,
    Authorize,
    Disconnect,
    Input,
    Chat,
    Snapshot,
    Ack
  }
}
