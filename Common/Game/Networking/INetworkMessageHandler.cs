using System;
using Common.Game.Networking.Messages;
using Lidgren.Network;

namespace Common.Game.Networking
{
  public interface INetworkMessageHandler
  {
    byte Code { get; }
    void Handler(NetIncomingMessage message);
  }
}

