using System;
using System.Collections.Generic;
using Common.Game;
using System.Linq;

namespace Common.Networking
{
  public class Snapshot
  {
    private static int nextSnapshotId = 0;

    public List<GameEntity> Entities { get; private set; }

    public int Id { get; private set; }

    public Snapshot (Dictionary<int, GameEntity> entities) : this( nextSnapshotId++, entities)
    {
    }

    public Snapshot (int id, Dictionary<int, GameEntity> entities)
    {
      Entities = new List<GameEntity> ();
      for (int i = 0; i < entities.Count; i++)
      {
        GameEntity copy = entities [i].Clone () as GameEntity;
      }
      Id = id;
    }
  }
}

