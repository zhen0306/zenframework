using System;
using Common.DataStructures;
using Common.Game;
using Server.Game;

namespace Common.Networking
{
  public class SnapshotRepository
  {
    public CircularBuffer<Snapshot> Snapshots { get; private set; }

    public SnapshotRepository ()
    {
      Snapshots = new CircularBuffer<Snapshot> (100);
    }

    public void CreateSnapshot(IGame game)
    {
      Snapshot currentSnapshot = new Snapshot (game.GameEntityManager.Entities);
      Snapshots.Insert (currentSnapshot);
    }
  }
}

