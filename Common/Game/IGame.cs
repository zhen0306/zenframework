﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Messaging;
using Common.Game.Managers;
using Common.Networking;
using Common.DataLoader;

namespace Common.Game
{
  public interface IGame
  {
    bool IsRunning { get; }

    MessageBusManager MessageBusManager { get; }
    GameEntityManager GameEntityManager { get; }
    SnapshotRepository SnapshotRepository { get; }
    DataStore DataStore { get; }

    void Start ();

    void Update (double deltaTime);

    void FixedUpdate ();

    void NetworkUpdate ();

    void End ();
  }
}
