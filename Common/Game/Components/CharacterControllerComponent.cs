using System;
using Common.Game;
using BEPUphysics;
using Common.Services;
using Common.Messaging;

namespace Common.Game.Components
{
  public class CharacterControllerComponent : IComponent
  {
    private GameEntity gameEntity;
    private ISpaceObject spaceObject;

    public CharacterControllerComponent(GameEntity gameEntity, ISpaceObject spaceObject)
    {
      this.gameEntity = gameEntity;
      this.spaceObject = spaceObject;

      gameEntity.MessageBusManager.AddEventHandler<GameEvents.InputEvent> (InputEventHandler);
    }

    private void InputEventHandler(MessageBus<GameEvents.InputEvent> message)
    {

    }

  }
}

