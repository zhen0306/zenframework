using System;

namespace Common.Log
{
  public interface ILog
  {
    void Debug(object message);
    void Info(object message);
    void Warn(object message);
    void Error(object message);
  }
}

