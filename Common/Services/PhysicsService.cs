﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Services;
using Common.Game;
using BEPUphysics;
using Common.Messaging;
using Common.Log;
using Common;

namespace Common.Services
{
  public class PhysicsService : IService, IFixedUpdateable
  {
    private IGame game;
    private ILog log;
    private Space space;

    public bool Enabled { get; set; }

    public PhysicsService (IGame game, ILog log, Space space)
    {
      log.Info ("Created Physics Service");
      this.game = game;
      this.log = log;
      this.space = space;
    }

    public void Start ()
    {
      game.MessageBusManager.AddEventHandler<GameEvents.AddPhysicsObject> (AddPhysicsObject);
      game.MessageBusManager.AddEventHandler<GameEvents.DestroyPhysicsObject> (RemovePhysicsObject);
    }

    public void FixedUpdate()
    {
      space.Update (TimeSettings.FixedUpdateRate / 1000.0f);
    }

    public void End ()
    {
            
    }

    private void AddPhysicsObject(MessageBus<GameEvents.AddPhysicsObject> message)
    {
      log.Info ("Added to space " + message.data.SpaceObject.GetType ());
      space.Add (message.data.SpaceObject);
    }

    private void RemovePhysicsObject(MessageBus<GameEvents.DestroyPhysicsObject> message)
    {
      log.Info ("Remove from space " + message.data.SpaceObject.GetType ());
      space.Remove (message.data.SpaceObject);
    }
  }
}
