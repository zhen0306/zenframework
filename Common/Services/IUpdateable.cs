﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Services
{
    public interface IUpdateable
    {
        void Update(double deltaTime);
    }
}
