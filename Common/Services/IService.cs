﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Services
{
  public interface IService
  {
    bool Enabled { get; set; }

    void Start ();

    void End ();
  }
}
