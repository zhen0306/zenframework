using System;

namespace Common.Services
{
  public interface INetworkUpdateable
  {
    void NetworkUpdate();
  }
}

