using System;

namespace Common.Services
{
  public interface IFixedUpdateable
  {
    void FixedUpdate();
  }
}

