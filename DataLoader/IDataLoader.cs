﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLoader
{
  public interface IDataLoader
  {
    void LoadAll<T> ();

    void LoadAll (Type type);

    void Load<T> (string path);

    void Load (Type type, string path);

    void Unload<T> (T data);

    void UnloadAll<T> ();

    void UnloadAll (Type type);

    void Save<T>(T data);
  }
}
