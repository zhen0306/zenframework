import System
from GameEvents import *
from Common.Messaging import MessageBus
from Utility import *

def Start():
	RegisterEvent(game, ActivateAbility, ActivateAbilityHandler);
	RegisterEvent(game, EndAbility, EndAbilityHandler);

def Update(deltaTime):
	return;

def End():
	RemoveEvent(game, ActivateAbility, ActivateAbilityHandler);
	RemoveEvent(game, EndAbility, EndAbilityHandler);
	return;

def ActivateAbilityHandler(msg):
	#activate ability
	Print(game, 'activating ability ' + msg.data.GUID);

def EndAbilityHandler(msg):
	Print(game, 'end ability');