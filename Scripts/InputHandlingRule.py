import System
from GameEvents import *
from Common.GameEvents import *
from Common.Messaging import MessageBus
from Utility import *
from Common import InputTypes

def Start():
	RegisterEvent(game, InputEvent, InputEventHandler);

def Update(deltaTime):
	return;

def End():
	RemoveEvent(game, InputEvent, InputEventHandler);
	return;

def InputEventHandler(msg):
	#trigger proper events based on input
	Print(game, 'Input from client ' + str(msg.data.ClientId));