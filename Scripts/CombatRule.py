import System
from GameEvents import *
from Common.Messaging import MessageBus
from Utility import *

def Start():
	RegisterEvent(game, DamageEvent, DamageEventHandler);

def Update(deltaTime):
	return;

def End():
	RemoveEvent(game, DamageEvent, DamageEventHandler);
	return;

def DamageEventHandler(msg):
	msg.data.Target.Attributes.Health -= msg.data.Amount;