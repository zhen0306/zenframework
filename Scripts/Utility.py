import System
from GameEvents import *
from Common.Messaging import MessageBus


def RegisterEvent(game, event, function):
	game.MessageBusManager.AddEventHandler[event](function);

def RemoveEvent(game, event, function):
	game.MessageBusManager.RemoveEventHandler[event](function);

def ContainsFlag(input, flag):
	return input & flag == flag;

def Print(game, message):
	game.MessageBusManager.SendMessage[ConsolePrint](
		MessageBus[ConsolePrint](
			ConsolePrint(message)));