import System
from GameEvents import *
from Common.Messaging import MessageBus
from Utility import *

entitiesPerWave = 10;
waveCount = 0;
waveTimer = 30;
waveStarted = False;
elapsedTime = 0.0;
nextSpawnTime = 0.0;

players = {};

def Start():
	for kvp in game.NetworkManager.Clients:
		PlayerSpawnEventHandler(MessageBus[PlayerSpawnEvent](PlayerSpawnEvent(kvp.Value, kvp.Value.GameEntity)));

	RegisterEvent(game, PlayerSpawnEvent, PlayerSpawnEventHandler);
	return;

def Update(deltaTime):
	global waveStarted, nextSpawnTime, entitiesPerWave, elapsedTime, waveTimer;
	if waveStarted:
		if elapsedTime >= nextSpawnTime:
			SpawnWave(entitiesPerWave);
			nextSpawnTime = elapsedTime + waveTimer;
		MonitorHealth();
		elapsedTime += deltaTime;
	return;

def End():
	return;

def SpawnWave(count):
	Print(game, "Spawning wave with " + str(count) + " count");
	return;

def MonitorHealth():
	isEveryoneDead = True;

	for client, entity in players.items():
		if entity.Attributes.Health > 0:
			isEveryoneDead = False;

	if isEveryoneDead:
		GameOver();

	return;

def PlayerSpawnEventHandler(message):
	global waveStarted

	client = message.data.Client;
	entity = message.data.GameEntity;
	players[client] = entity;
	Print(game, "Spawned player");

	if waveStarted == False:
		waveStarted = True;
		Print(game, "Starting Game");

def GameOver():
	global waveStarted

	waveStarted = False;
	Print(game, "Game Over");
	return;