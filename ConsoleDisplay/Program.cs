﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Server.Application.Modules;
using Common.Game;
using Server.Application;
using System.Threading;
using log4net.Config;
using log4net;

class Program
{
  static void Main (string[] args)
  {
    ApplicationRoot root = new ApplicationRoot ();
    root.Main ();
  }
}

