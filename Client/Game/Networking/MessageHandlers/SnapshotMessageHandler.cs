using System;
using Common.Game.Networking;
using Lidgren.Network;
using Common.Game.Networking.Messages;
using Common.Game;
using Common.Log;
using System.Collections.Generic;
using System.Linq;
using Common.Networking;
using Common.GameEvents;

namespace Client.Game.Networking.MessageHandlers
{
  public class SnapshotMessageHandler : INetworkMessageHandler
  {
    public byte Code {
      get {
        return (byte)MessageTypes.Snapshot;
      }
    }

    private IGame game;
    private Dictionary<EntityTypes, IEntityFactory> factories;
    private ILog log;
    private int lastAck = 0;

    public SnapshotMessageHandler (IGame game, ILog log, IEnumerable<IEntityFactory> factories)
    {
      this.game = game;
      this.factories = factories.ToDictionary (x => x.Type);
      this.log = log;
    }

    #region INetworkMessageHandler implementation

    public void Handler (NetIncomingMessage message)
    {
      int snapShotId = message.ReadInt32 ();

      //don't process any new messages that are older then the last ack
      if (snapShotId > lastAck)
      {
        int clientInputAck = message.ReadInt32 ();
        int count = message.ReadInt32 ();

        Dictionary<int, GameEntity> entities = new Dictionary<int, GameEntity> ();
        for (int i = 0; i < count; i++)
        {
          int networkId = message.ReadInt32 ();
          EntityTypes type = (EntityTypes)message.ReadByte ();
          GameEntity entity;
          if (!game.GameEntityManager.Entities.ContainsKey (networkId))
          {
            entity = factories [type].CreateEntity (networkId);
            game.GameEntityManager.AddEntity (networkId, entity);
            log.Info (networkId + " " + snapShotId);
          } else
          {
            entity = game.GameEntityManager.Entities [networkId];
          }
          entity.GetComponent<NetworkComponent> ().Deserialize (message);
          entities.Add (networkId, entity);

        }
        Snapshot snapshot = new Snapshot (snapShotId, entities);
        game.SnapshotRepository.Snapshots.Insert (snapshot);

        //send ack back to server
        game.MessageBusManager.SendMessage (new AckEvent (snapShotId));
        lastAck = snapShotId;
      }
    }
    #endregion
  }
}

