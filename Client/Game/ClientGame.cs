using System;
using Common.Game;
using Common.Messaging;
using Ninject;
using Common.Services;
using System.Collections.Generic;
using Common.Log;
using Common.Game.Managers;
using Common.Networking;
using Common.DataLoader;

namespace Client.Game
{
  public class ClientGame : IGame
  {
    public bool IsRunning {
      get {
        return true;
      }
    }

    public GameEntityManager GameEntityManager { get; private set; }

    public MessageBusManager MessageBusManager { get; private set; }

    public DataStore DataStore { get; private set; }

    private ILog log;

    [Inject]
    public List<IService> Services { get; private set; }

    [Inject]
    public SnapshotRepository SnapshotRepository { get; private set; }

    public List<IUpdateable> UpdatableService { get; private set; }
    public List<IFixedUpdateable> FixedUpdatableService { get; private set; }
    public List<INetworkUpdateable> NetworkUpdatableService { get; private set; }

    public ClientGame (MessageBusManager messageBusManager, GameEntityManager gameEntityManager, DataStore dataStore, ILog log)
    {
      this.MessageBusManager = messageBusManager;
      this.GameEntityManager = gameEntityManager;
      this.DataStore = dataStore;

      this.log = log;
      UpdatableService = new List<IUpdateable> ();
      FixedUpdatableService = new List<IFixedUpdateable> ();
      NetworkUpdatableService = new List<INetworkUpdateable> ();
    }

    public void Start ()
    {
      log.Info ("ClientGame Started");
      foreach (IService service in Services)
      {
        if (service is IUpdateable)
        {
          UpdatableService.Add (service as IUpdateable);
        }
        if (service is IFixedUpdateable)
        {
          FixedUpdatableService.Add (service as IFixedUpdateable);
        }
        if (service is INetworkUpdateable)
        {
          NetworkUpdatableService.Add (service as INetworkUpdateable);
        }
      }

      for (int i = 0; i < Services.Count; i++)
      {
        Services [i].Start ();
      }
    }

    public void Update (double deltaTime)
    {
      for (int i = 0; i < UpdatableService.Count; i++)
      {
        UpdatableService [i].Update (deltaTime);
      }
    }

    public void FixedUpdate ()
    {
      for (int i = 0; i < FixedUpdatableService.Count; i++)
      {
        FixedUpdatableService [i].FixedUpdate ();
      }
    }

    public void NetworkUpdate ()
    {
      for (int i = 0; i < NetworkUpdatableService.Count; i++)
      {
        NetworkUpdatableService [i].NetworkUpdate ();
      }
    }

    public void End ()
    {
      for (int i = 0; i < Services.Count; i++)
      {
        Services [i].End ();
      }
    }
  }
}

