using System;
using Common.Game;

namespace Client
{
  public interface IEntityFactory
  {
    EntityTypes Type { get; }
    GameEntity CreateEntity(int id);
  }
}

