using System;
using Common.Game;

namespace Client
{
  public abstract class NetworkComponent : IComponent
  {
    public int NetworkId { get; private set; }

    public NetworkComponent (int networkId)
    {
      NetworkId = networkId;
    }

    public abstract void Deserialize(Lidgren.Network.NetIncomingMessage message);
  }
}

