using System;
using Common.Services;
using Lidgren.Network;
using System.Net;
using Common.Game;
using Common.Log;
using Common.GameEvents;
using Common.Messaging;
using System.Collections.Generic;
using Common.Game.Networking.Messages;
using Common.Game.Networking;
using System.Linq;

namespace Client.Game.Services
{
  public class NetworkService : IService, INetworkUpdateable
  {
    private IGame game;
    private ILog log;
    private NetClient client;
    private NetConnection connection;
    private Dictionary<byte, INetworkMessageHandler> handlers;

    private int ack;

    public bool Enabled { get; set; }

    public NetworkService (NetClient client, IGame game, ILog log, IEnumerable<INetworkMessageHandler> networkMessageHandles)
    {
      this.game = game;
      this.log = log;
      this.client = client;
      log.Info ("Created Network Service");
      this.handlers = networkMessageHandles.ToDictionary (x => x.Code);
    }

    public void Start ()
    {
      ack = 0;
      client.Start ();
      connection = client.Connect (new IPEndPoint (NetUtility.Resolve ("127.0.0.1"), 50003));
      game.MessageBusManager.AddEventHandler<InputEvent> (InputEventHandler);
      game.MessageBusManager.AddEventHandler<AckEvent> (AckEventHandler);
    }

    public void End ()
    {
    }

    private void InputEventHandler(MessageBus<InputEvent> message)
    {
      NetOutgoingMessage outgoingMessage = client.CreateMessage ();
      outgoingMessage.Write ((byte)MessageTypes.Input);
      outgoingMessage.Write (ack);
      outgoingMessage.Write ((int)message.data.Input);
      outgoingMessage.Write (message.data.DeltaTime);
      client.SendMessage (outgoingMessage, NetDeliveryMethod.ReliableOrdered);
      ack++;
    }

    private void AckEventHandler(MessageBus<AckEvent> message)
    {
      NetOutgoingMessage ackMessage = client.CreateMessage ();
      ackMessage.Write ((byte)MessageTypes.Ack);
      ackMessage.Write (message.data.SnapshotId);
      client.SendMessage (ackMessage, NetDeliveryMethod.ReliableOrdered);
    }

    public void NetworkUpdate ()
    {
      NetIncomingMessage im;
      while ((im = client.ReadMessage()) != null)
      {
        // handle incoming message
        switch (im.MessageType)
        {
          case NetIncomingMessageType.DebugMessage:
          case NetIncomingMessageType.ErrorMessage:
          case NetIncomingMessageType.WarningMessage:
          case NetIncomingMessageType.VerboseDebugMessage:
          string text = im.ReadString();
          log.Debug(text);
          break;
        case NetIncomingMessageType.StatusChanged:
          NetConnectionStatus status = (NetConnectionStatus)im.ReadByte ();

          if (status == NetConnectionStatus.Connected)
            log.Info ("Connected");


          string reason = im.ReadString();
          log.Info(status.ToString() + ": " + reason);

          break;
        case NetIncomingMessageType.Data:
          byte code = im.ReadByte ();
          handlers [code].Handler (im);

          break;
          default:
          log.Info("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes");
          break;
        }
      }
    }
  }
}

