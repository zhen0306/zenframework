using System;
using Common.Services;
using Client.Game.Services;
using Lidgren.Network;
using Common.Game.Networking;
using Common.GameEvents;
using Client.Game.Networking.MessageHandlers;

namespace Client
{
  public class NetworkInstaller: Ninject.Modules.NinjectModule
  {
    public override void Load ()
    {
      Bind<IService> ().To<NetworkService> ().InSingletonScope ();
      Bind<NetClient> ().ToProvider<LidgrenClientProvider> ().InSingletonScope();
      Bind<INetworkMessageHandler> ().To<SnapshotMessageHandler> ().InSingletonScope();
    }
  }
}

