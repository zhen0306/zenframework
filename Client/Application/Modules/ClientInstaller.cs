using System;
using Ninject;
using Common.Game;
using Client.Game;
using Common.Messaging;
using DataLoader;
using Common.Services;
using Lidgren.Network;
using Client.Game.Services;
using Common.Networking;
using Common.DataLoader;

namespace Client.Application.Modules
{
  public class ClientInstaller : Ninject.Modules.NinjectModule
  {
    public override void Load ()
    {
      Bind<IGame> ().To<ClientGame> ().InSingletonScope ();
      Bind<MessageBusManager> ().ToSelf ().InSingletonScope();
      Bind<IService> ().To<PhysicsService> ().InSingletonScope ();
      Bind<SnapshotRepository> ().ToSelf ().InSingletonScope ();
      Bind<DataStore> ().ToSelf ().InSingletonScope();
    }
  }
}

