using System;
using Ninject.Activation;
using Lidgren.Network;

namespace Client
{
  public class LidgrenClientProvider: Provider<NetClient>
  {

    protected override NetClient CreateInstance (IContext context)
    {
      NetPeerConfiguration config = new NetPeerConfiguration ("Server");
      config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
      return new NetClient (config);
    }
  }
}

