﻿using UnityEngine;
using System.Collections;
using System.Linq;

[RequireComponent(typeof(MeshCollider))]
public class BEPUMeshCollider : BEPUCollider
{
		private BEPUphysics.BroadPhaseEntries.StaticMesh mesh;

		void Start ()
		{
				MeshCollider meshCollider = collider as MeshCollider;
				BEPUutilities.Vector3[] verts = meshCollider.sharedMesh.vertices.Select (x => x.ToBEPU ()).ToArray();
				int[] indices = meshCollider.sharedMesh.GetIndices (0);
				mesh = new BEPUphysics.BroadPhaseEntries.StaticMesh (
						verts, 
						indices,
						new BEPUutilities.AffineTransform (gameObject.transform.position.ToBEPU ()));
				RegisterPhysicsObject (mesh);

		}

		void OnDestroy ()
		{
				DestroyPhysicsObject (mesh);
		}
}
