﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class BEPUBoxCollider : BEPUCollider
{
		private BEPUphysics.Entities.Prefabs.Box box;
		public bool isKinematic = false;

		void Start ()
		{
				box = new BEPUphysics.Entities.Prefabs.Box (
						gameObject.transform.position.ToBEPU () + collider.bounds.center.ToBEPU (), 
						collider.bounds.extents.x * 2, 
						collider.bounds.extents.y * 2, 
						collider.bounds.extents.z * 2, 
						rigidbody.mass);
				if (isKinematic) {
						box.BecomeKinematic ();
				}
				RegisterPhysicsObject (box);
				
		}

		void Update ()
		{

				box.Mass = rigidbody.mass;
				box.Material.Bounciness = 0;
				box.HalfHeight = collider.bounds.extents.y;
				box.HalfWidth = collider.bounds.extents.x;
				box.HalfLength = collider.bounds.extents.z;

				if (Input.GetButton ("Fire1")) {
						box.LinearVelocity = (Vector3.up * 10).ToBEPU ();
				}
		}

		void FixedUpdate ()
		{
				gameObject.transform.position = box.Position.ToUnity ();
				gameObject.transform.rotation = box.Orientation.ToUnity ();
		}

		void OnDestroy ()
		{
				DestroyPhysicsObject (box);
		}
}
