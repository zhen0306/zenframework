﻿using UnityEngine;
using System.Collections;
using BEPUphysics;

public class BEPUCollider : MonoBehaviour
{
	void Awake ()
	{
		if(rigidbody != null)
			rigidbody.isKinematic = true;
	}

	public void RegisterPhysicsObject(ISpaceObject spaceObject)
	{
		/*
		ApplicationRoot.game.MessageBusManager.SendMessage<Common.GameEvents.AddPhysicsObject> (
			new Common.GameEvents.AddPhysicsObject (spaceObject));
			*/
	}

	public void DestroyPhysicsObject(ISpaceObject spaceObject)
	{
		/*
		ApplicationRoot.game.MessageBusManager.SendMessage<Common.GameEvents.DestroyPhysicsObject> (
			new Common.GameEvents.DestroyPhysicsObject (spaceObject));*/
	}
}
