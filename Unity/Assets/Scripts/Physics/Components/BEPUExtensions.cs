﻿using BEPUutilities;
using UnityEngine;
using System.Collections;

public static class BEPUextensions
{
		public static BEPUutilities.Vector3 ToBEPU (this UnityEngine.Vector3 vec)
		{
				return new BEPUutilities.Vector3 (vec.x, vec.y, vec.z);
		}

		public static UnityEngine.Vector3 ToUnity (this BEPUutilities.Vector3 vec)
		{
				return new UnityEngine.Vector3 (vec.X, vec.Y, vec.Z);
		}

		public static BEPUutilities.Quaternion ToBEPU (this UnityEngine.Quaternion rot)
		{
				return new BEPUutilities.Quaternion (rot.x, rot.y, rot.z, rot.w);
		}

		public static UnityEngine.Quaternion ToUnity (this BEPUutilities.Quaternion rot)
		{
				return new UnityEngine.Quaternion (rot.X, rot.Y, rot.Z, rot.W);
		}
}