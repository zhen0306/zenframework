﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class BEPUSphereCollider : BEPUCollider
{
		private BEPUphysics.Entities.Prefabs.Sphere sphere;

		void Start ()
		{
				sphere = new BEPUphysics.Entities.Prefabs.Sphere (
						gameObject.transform.position.ToBEPU (), 
						(collider as SphereCollider).radius,
						rigidbody.mass);
				RegisterPhysicsObject (sphere);

		}

		void FixedUpdate ()
		{
				gameObject.transform.position = sphere.Position.ToUnity ();
				gameObject.transform.rotation = sphere.Orientation.ToUnity ();
		}

		void OnDestroy ()
		{
				DestroyPhysicsObject (sphere);
		}
}
