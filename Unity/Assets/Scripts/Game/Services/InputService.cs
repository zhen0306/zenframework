﻿using UnityEngine;
using System.Collections;
using Common.Services;
using Common.Game;
using Common;
using Common.GameEvents;
using Common.Log;

public class InputService : IService, IUpdateable
{
		private IGame game;
		private InputTypes currentInputState;
	private ILog log;
	
	public bool Enabled { get; set; }


		public InputService (IGame game, ILog log)
		{
				log.Info ("Registered Input Service");
				this.game = game;
				this.log = log;
		}

		public void Start ()
		{
				log.Info ("starting log");
		}

		public void End ()
		{

		}

		public void Update (double deltaTime)
		{
				// Use bitwise and a ternary to "OR" input states into the object
				var previousInputState = currentInputState;
				currentInputState = InputTypes.None;
				currentInputState |= (Input.GetKey (KeyCode.W) ? InputTypes.Forward : InputTypes.None);
				currentInputState |= (Input.GetKey (KeyCode.S) ? InputTypes.Back : InputTypes.None);
				currentInputState |= (Input.GetKey (KeyCode.A) ? InputTypes.Left : InputTypes.None);
				currentInputState |= (Input.GetKey (KeyCode.D) ? InputTypes.Right : InputTypes.None);
				currentInputState |= (Input.GetKey (KeyCode.Space) ? InputTypes.Jump : InputTypes.None);
				currentInputState |= (Input.GetButton ("Fire1") ? InputTypes.PrimaryFire : InputTypes.None);
				currentInputState |= (Input.GetButton ("Fire2") ? InputTypes.SecondaryFire : InputTypes.None);

				if (previousInputState != currentInputState) {
						game.MessageBusManager.SendMessage (new InputEvent (currentInputState, (float)deltaTime));
				}

		}
}
