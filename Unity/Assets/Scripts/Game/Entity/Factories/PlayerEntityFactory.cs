﻿using System;
using Common.Game;

namespace Client
{
  public class PlayerEntityFactory : IEntityFactory
  {
    public PlayerEntityFactory ()
    {
    }

    #region IEntityFactory implementation

    public GameEntity CreateEntity (int id)
    {
      return new PlayerEntity (id);
    }

    public Common.Game.EntityTypes Type {
      get {
        return Common.Game.EntityTypes.Player;
      }
    }

    #endregion
  }
}

