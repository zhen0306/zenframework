﻿using System;
using Common.Game;

namespace Client
{
	public class PlayerEntity : GameEntity
	{
    #region implemented abstract members of GameEntity

		public override EntityTypes Type {
			get {
				return EntityTypes.Player;
			}
		}
    #endregion

		public PlayerNetworkComponent NetworkComponent { get; private set; }
		public GameObjectMediator GameObjectMediator { get; private set; }
		
		public int Health = 0;
		public int MaxHealth = 100;

		public PlayerEntity (int id)
		{
			NetworkComponent = AddComponent (new PlayerNetworkComponent (this, id));
			GameObjectMediator = AddComponent(new GameObjectMediator(this));
		}

		public class PlayerNetworkComponent : NetworkComponent
		{
			private PlayerEntity entity;

			public PlayerNetworkComponent (PlayerEntity entity, int id) : base(id)
			{
				this.entity = entity;
			}

      #region implemented abstract members of NetworkComponent
			public override void Deserialize (Lidgren.Network.NetIncomingMessage message)
			{
				entity.Health = message.ReadInt32 ();
				entity.MaxHealth = message.ReadInt32 ();
			}
      #endregion
		}
	}
}

