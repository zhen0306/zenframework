﻿using UnityEngine;
using System.Collections;
using Common.Game;

public class GameObjectMediator : IComponent 
{
	public GameEntity GameEntity { get; private set; }
	public GameObject GameObject { get; private set; }
	
	public GameObjectMediator(GameEntity entity)
	{
		GameEntity = entity;
		GameObject = new GameObject("Mediator");	
	}
}
