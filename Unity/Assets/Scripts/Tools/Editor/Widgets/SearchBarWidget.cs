using System;
using Common.Messaging;
using UnityEditor;

public class SearchBarWidget : BaseWidget
{
	public string SearchString { get; private set; }
	
	public SearchBarWidget (MessageBusManager messageBusManager) : base(messageBusManager)
	{
		SearchString = "";
	}
	
	public override void OnGUI()
	{
		SearchString = EditorGUILayout.TextField(SearchString);
	}
}


