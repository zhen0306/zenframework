using System;
using System.Collections.Generic;
using Common.Messaging;
using UnityEditor;
using UnityEngine;
using Common.Game.Data;
using DataLoader.Data;
using Common;

public enum ToolbarEnum
{
	All,
	Abilities,
	Characters,
	Weapons,
	Maps
}

public class DataToolbar : BaseWidget
{
	private ToolbarEnum selectedToolbar;
	
	public DataToolbar (MessageBusManager messageBusManager) : base(messageBusManager)
	{
		messageBusManager.AddEventHandler<UIEvent.SelectedCreateData>(ToolbarEventHandler);
	}
	
	public override void OnGUI()
	{
		GUI.backgroundColor = Color.black;
		EditorGUILayout.BeginHorizontal("box");
		if(GUILayout.Button("Create", EditorStyles.toolbarButton, GUILayout.MaxWidth(100)))
		{
			GenericMenu menu = new GenericMenu();

			var names = System.Enum.GetNames(typeof(ToolbarEnum));
			for(int i = 1; i < names.Length; i++)
			{	
				int id = i;
				menu.AddItem(new GUIContent(names[i]), false, ()=>
				{
					MessageBusManager.SendMessage(new UIEvent.SelectedCreateData((ToolbarEnum)id));
				});
			}
			menu.ShowAsContext();
		}
		
		if(GUI.changed)
			GUI.changed = false;
		
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
		GUI.backgroundColor = Color.white;
	}
	
	private void ToolbarEventHandler (MessageBus<UIEvent.SelectedCreateData> message)
	{
		BaseData data = null;
		switch(message.data.Toolbar)
		{
		case ToolbarEnum.Abilities:
			data = DatabaseRepository.DataStore.CreateData<AbilityData>();
			break;
		case ToolbarEnum.Characters:
			data = DatabaseRepository.DataStore.CreateData<CharacterClassData>();
			break;
		case ToolbarEnum.Weapons:
			data = DatabaseRepository.DataStore.CreateData<WeaponData>();
			break;
		case ToolbarEnum.Maps:
			data = DatabaseRepository.DataStore.CreateData<MapData>();
			break;
		default:
			break;
		}
		
		if(data != null)
		{
			MessageBusManager.SendMessage(new UIEvent.CreatedData(data));
		}
	}
}


