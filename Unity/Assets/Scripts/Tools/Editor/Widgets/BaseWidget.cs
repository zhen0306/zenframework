using System;
using Common.Messaging;


public abstract class BaseWidget
{
	public bool Enabled { get; set; }
	public MessageBusManager MessageBusManager { get; private set; }
	
	public BaseWidget (MessageBusManager messageBusManager)
	{
		this.MessageBusManager = messageBusManager;
	}
	
	public abstract void OnGUI();
}


