using System;
using Common.Messaging;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using DataLoader.Data;
using Common.Game.Data;

public class DataInspector : BaseWidget
{
	private Dictionary<Type, BaseInspector> Inspectors;
	private BaseData currentData;
	
	public DataInspector (MessageBusManager messageBusManager) : base(messageBusManager)
	{
		Inspectors = new Dictionary<Type, BaseInspector>();
		//use reflection to get all inspectors and add them
		Assembly assembly = Assembly.GetAssembly(typeof(BaseInspector));
		var inspectors = assembly.GetTypes().Where((t)=>typeof(BaseInspector).IsAssignableFrom(t) && t != typeof(BaseInspector));
		foreach(Type inspector in inspectors)
		{
			BaseInspector instance = (BaseInspector)Activator.CreateInstance(inspector);
			Inspectors.Add(instance.DisplayType, instance);
		}
		
		messageBusManager.AddEventHandler<UIEvent.SelectData>(SelectDataEventHandler);
	}
	
	public override void OnGUI()
	{
		if(currentData != null && Inspectors.ContainsKey(currentData.GetType()))
		{
			Inspectors[currentData.GetType()].Data = currentData;
			Inspectors[currentData.GetType()].OnGUI();
		}
	}

	private void SelectDataEventHandler (MessageBus<UIEvent.SelectData> message)
	{
		currentData = DatabaseRepository.DataStore.GetData(message.data.GUID);
	}
}


