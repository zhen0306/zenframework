using System;
using DataLoader.Data;
using UnityEditor;
using Common.Game.Data;
using UnityEngine;

public class AbilityInspector : BaseInspector
{
	public override Type DisplayType {
		get {
			return typeof(AbilityData);
		}
	}
	
	public override void OnGUI ()
	{
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Save"))
		{
			DatabaseRepository.DataLoader.Save(Data as AbilityData);
		}
		
		EditorGUILayout.EndHorizontal();
		Data.Name = EditorGUILayout.TextField("Name", Data.Name);
	}
}


