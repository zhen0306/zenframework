using System;
using DataLoader.Data;
using UnityEditor;
using Common.Game.Data;
using UnityEngine;

public class WeaponInspector : BaseInspector
{
	public override Type DisplayType {
		get {
			return typeof(WeaponData);
		}
	}
	
	public override void OnGUI ()
	{
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Save"))
		{
			DatabaseRepository.DataLoader.Save(Data as WeaponData);
		}
		
		EditorGUILayout.EndHorizontal();
		Data.Name = EditorGUILayout.TextField("Name", Data.Name);
	}
}


