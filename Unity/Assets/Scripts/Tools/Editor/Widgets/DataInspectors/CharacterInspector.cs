using System;
using DataLoader.Data;
using UnityEditor;
using Common.Game.Data;
using UnityEngine;

public class CharacterInspector : BaseInspector
{
	public override Type DisplayType {
		get {
			return typeof(CharacterClassData);
		}
	}
	
	public override void OnGUI ()
	{
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Save"))
		{
			DatabaseRepository.DataLoader.Save(Data as CharacterClassData);
		}
		
		EditorGUILayout.EndHorizontal();
		Data.Name = EditorGUILayout.TextField("Name", Data.Name);
	}
}


