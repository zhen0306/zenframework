using System;
using DataLoader.Data;
using UnityEditor;
using Common.Game.Data;
using UnityEngine;
using Common;

public class MapInspector : BaseInspector
{
	public override Type DisplayType {
		get {
			return typeof(MapData);
		}
	}
	
	public override void OnGUI ()
	{
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Save"))
		{
			DatabaseRepository.DataLoader.Save(Data as MapData);
		}
		
		EditorGUILayout.EndHorizontal();
		Data.Name = EditorGUILayout.TextField("Name", Data.Name);
	}
}


