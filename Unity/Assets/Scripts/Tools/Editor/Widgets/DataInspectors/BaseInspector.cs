using System;
using DataLoader.Data;
using UnityEngine;


public abstract class BaseInspector
{
	public abstract Type DisplayType { get; }
	
	public BaseData Data;
	
	public abstract void OnGUI();
}