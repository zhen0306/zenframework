using System;
using JsonFx;
using Common.DataLoader;
using Common.Game.Data;
using System.Reflection;
using DataLoader.Data;
using System.Linq;
using DataLoader;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class DatabaseRepository
{
	public static DataStore DataStore { get; private set; }
	public static IDataLoader DataLoader { get; private set; }
	
	static DatabaseRepository ()
	{
		DataStore = new DataStore(new UnityLog());
		DataLoader = new EditorDataLoader(DataStore, new UnityLog());
		
		//use reflection to get all data types
		Assembly assembly = Assembly.GetAssembly(typeof(AbilityData));
		var DataTypes = assembly.GetTypes().Where(t=>typeof(BaseData).IsAssignableFrom(t) && t != typeof(BaseData));
		foreach(var dataType in DataTypes)
		{
			DataLoader.LoadAll(dataType);
		}
	}
}


