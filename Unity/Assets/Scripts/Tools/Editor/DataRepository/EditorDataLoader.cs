using System;
using DataLoader;
using Common.DataLoader;
using JsonFx;
using UnityEditor;
using UnityEngine;
using System.IO;
using JsonFx.Json;
using DataLoader.Data;
using Common.Log;

public class EditorDataLoader : IDataLoader
{
	public DataStore DataStore { get; private set; }
	private string directoryPath;
	private ILog log;
	
	public EditorDataLoader(DataStore dataStore, ILog log)
	{
		DataStore = dataStore;
		directoryPath = Application.dataPath + "/Data/";
		this.log = log;
		log.Info(directoryPath);
	}
	
	public void LoadAll<T> ()
	{
		LoadAll(typeof(T));
	}
	
	public void LoadAll(Type type)
	{
		log.Info("Loading all data of type " + type.Name);
		//load everything in directory
		string path = directoryPath + type.Name;
		if(!Directory.Exists(path))
			return;
		
		var files = Directory.GetFiles(path,"*.json");
		foreach(string file in files)
		{
			Load(type, file);
		}
	}

	public void Load<T> (string path)
	{
		Load (typeof(T),path);
	}
	
	public void Load(Type type, string path)
	{
		log.Info("Loading file " + path);
		string fullPath = path;
		string text = File.ReadAllText(fullPath);
		var reader = new JsonReader();
		BaseData data = reader.Read(text, type) as BaseData;
		DataStore.AddData(data);
	}

	public void Unload<T> (T data)
	{
	}

	public void UnloadAll<T> ()
	{
	}
	
	public void UnloadAll(Type type)
	{
	}

	public void Save<T>(T data)
	{
		BaseData baseData = data as BaseData;
		var writer = new JsonWriter();
		string serialized = writer.Write(data);
		string path = directoryPath + typeof(T).Name + "/" + baseData.GUID + ".json";
		
		File.WriteAllText(path, serialized);
		AssetDatabase.Refresh();
	}
}


