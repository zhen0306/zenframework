﻿using UnityEngine;
using UnityEditor;

using System.Collections;
using Common.Messaging;

public class BaseDataEditorWindow : EditorWindow 
{
	public static DataListPanel DataList { get; private set; }
	public static DataToolbar Toolbar { get; private set; }
	public static DataInspector Inspector { get; private set; }
	public static MessageBusManager MessageBusManager { get; private set; }
	
	[MenuItem("Zen/BaseEditorWindow")]
	public static void Init()
	{
		var window = EditorWindow.GetWindow<BaseDataEditorWindow>();
		window.Show();
	}
	
	static BaseDataEditorWindow()
	{
		MessageBusManager = new MessageBusManager();
		DataList = new DataListPanel(MessageBusManager);
		Toolbar = new DataToolbar(MessageBusManager);
		Inspector = new DataInspector(MessageBusManager);
	}
	
	void OnGUI()
	{	
		Toolbar.OnGUI();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.BeginVertical(GUILayout.Width(Screen.width * 0.3f));
		DataList.OnGUI();
		EditorGUILayout.EndVertical();		
		EditorGUILayout.BeginVertical(GUILayout.Width(Screen.width * 0.7f));
		Inspector.OnGUI();
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();
	}
}
