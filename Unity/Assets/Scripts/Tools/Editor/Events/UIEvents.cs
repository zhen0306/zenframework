using System;
using DataLoader.Data;

namespace UIEvent
{
	public class SelectToolbar : EventArgs
	{
		public ToolbarEnum Toolbar { get; private set; }
		
		public SelectToolbar(ToolbarEnum toolbar)
		{
			Toolbar = toolbar;
		}
	}
	
	public class SelectData: EventArgs
	{
		public string GUID { get; private set; }
		
		public SelectData(string guid)
		{
			GUID = guid;
		}
	}
	
	public class SelectedCreateData:EventArgs
	{
		public ToolbarEnum Toolbar { get; private set; }
		
		public SelectedCreateData(ToolbarEnum toolbar)
		{
			Toolbar = toolbar;
		}
	}
	
	public class CreatedData : EventArgs
	{
		public BaseData Data { get; private set; }
		
		public CreatedData(BaseData data)
		{
			Data = data;
		}
	}
}
