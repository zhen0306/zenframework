﻿using UnityEngine;
using System.Collections;
using Common.Game;
using Ninject;
using Client.Application.Modules;
using Common.Applications.Modules;
using Common.Log;
using Client;

public class ApplicationRoot : MonoBehaviour
{

		public static IGame game { get; private set; }

		void Awake ()
		{
				StandardKernel kernel = new StandardKernel (new NinjectSettings (), 
		                                            new ClientInstaller (), 
		                                            new PhysicsInstaller (),
		                                            new NetworkInstaller(),
                                                new FactoryInstaller(),
		                                            new UnityClientInstaller());
				kernel.Bind<ILog> ().To<UnityLog> ();
				game = kernel.Get<IGame> ();
				
		}

		void Start ()
		{
				game.Start ();
		}

		void Update ()
		{
				game.Update ((long)Time.deltaTime);
				NetworkUpdate ();
		}

		void FixedUpdate ()
		{
				game.FixedUpdate ();
		}

		void NetworkUpdate ()
		{
				game.NetworkUpdate ();
		}

		void OnDestroy ()
		{
				game.End ();
		}
}
