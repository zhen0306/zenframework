﻿using UnityEngine;
using System.Collections;
using Common.Services;
using Client;

public class UnityClientInstaller : Ninject.Modules.NinjectModule
{
	#region implemented abstract members of NinjectModule
	public override void Load ()
	{
		Bind<IService>().To<InputService>();
		Bind<IEntityFactory>().To<PlayerEntityFactory>();
	}
	#endregion
}
