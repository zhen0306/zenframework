using System;
using Ninject;
using Ninject.Activation;
using Lidgren.Network;

namespace Server
{
  public class LidgrenServerNetworkProvider : Provider<NetServer>
  {

    protected override NetServer CreateInstance (IContext context)
    {
      NetPeerConfiguration config = new NetPeerConfiguration ("Server");
      config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
      config.Port = 50003;
      return new NetServer (config);
    }
  }
}

