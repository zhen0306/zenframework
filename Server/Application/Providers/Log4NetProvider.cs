using System;
using log4net;
using Ninject.Activation;
using log4net.Config;
using log4net.Appender;

namespace Server
{
  public class Log4NetProvider : Provider<ILog>
  {
    public Log4NetProvider ()
    {
    }

    #region implemented abstract members of Provider

    protected override ILog CreateInstance (IContext context)
    {
      log4net.Config.XmlConfigurator.Configure();
      return LogManager.GetLogger (context.Request.Target.Member.ReflectedType);
    }

    #endregion
  }
}

