using System;
using Ninject.Activation;
using Microsoft.Scripting.Hosting;
using System.Reflection;
using System.IO;
using IronPython.Hosting;
using System.Collections.Generic;

namespace Server
{
  public class RuntimeScriptingProvider : Provider<ScriptEngine>
  {

    #region implemented abstract members of Provider

    protected override ScriptEngine CreateInstance (IContext context)
    {
      string ScriptPath = Assembly.GetEntryAssembly ().Location + "/../../../../Scripts/";
      string dir = Path.GetDirectoryName(Path.GetFullPath (ScriptPath));  
      string[] dirs = Directory.GetDirectories (dir, "*", SearchOption.AllDirectories);

      var scriptEngine = Python.CreateEngine ();

      ICollection<string> paths = scriptEngine.GetSearchPaths();

      if (!string.IsNullOrWhiteSpace (dir))
      {
        paths.Add (dir);
      }
      for (int i = 0; i < dirs.Length; i++)
      {
        if (!string.IsNullOrWhiteSpace (dirs[i]))
        {
          paths.Add (dirs[i]);
        }
      }

      scriptEngine.SetSearchPaths(paths);

      return scriptEngine;
    }

    #endregion
  }
}

