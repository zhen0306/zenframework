using System;
using Common.Game.Networking;
using Server.Game.Networking.MessageHandlers;

namespace Server
{
  public class NetworkMessageInstaller : Ninject.Modules.NinjectModule
  {
    public override void Load ()
    {
      Bind<INetworkMessageHandler>().To<ClientAckHandler>();
      Bind<INetworkMessageHandler> ().To<InputNetworkMessageHandler> ();
      Bind<INetworkMessageHandler> ().To<ConnectMessageHandler> ();
      Bind<INetworkMessageHandler> ().To<DisconnectMessageHandler> ();
      Bind<INetworkMessageHandler> ().To<ConnectAuthorizeHandler> ();
    }
  }
}

