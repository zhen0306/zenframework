﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Common.Game;
using Server.Game;
using Common.Messaging;
using DataLoader;
using Common.Services;
using Game.Server.Services;
using Server.Game.Services;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;
using Lidgren.Network;
using log4net;
using Server.Game.Networking;
using Common.Game.Managers;
using Ninject.Extensions.Factory;
using Server.Game.Entities;
using Common.Networking;
using Common.DataLoader;

namespace Server.Application.Modules
{
  public class ServerInstaller : Ninject.Modules.NinjectModule
  {
    public override void Load ()
    {
      Bind<IGame> ().To<ServerGame> ().InSingletonScope ();
      Bind<MessageBusManager> ().ToSelf ();
      Bind<IDataLoader> ().To<DataLoader.FileDataLoader> ().InSingletonScope ();
      Bind<IService> ().To<DataLoaderService> ().InSingletonScope ();
      Bind<IService> ().To<RuntimeScriptingEngineService> ().InSingletonScope ();
      Bind<IService> ().To<NetworkService> ().InSingletonScope ();
      Bind<IService> ().To<PhysicsService> ().InSingletonScope ();
      Bind<IService> ().To<GameEntityService> ().InSingletonScope ();
      Bind<ScriptEngine> ().ToProvider<RuntimeScriptingProvider>();
      Bind<NetServer> ().ToProvider<LidgrenServerNetworkProvider> ().InSingletonScope();
      Bind<log4net.ILog> ().ToProvider<Log4NetProvider> ().InSingletonScope();
      Bind<Common.Log.ILog> ().To<CommonLogToLog4Net> ().InSingletonScope();
      Bind<SnapshotRepository> ().ToSelf ().InSingletonScope ();
      Bind<GameEntityManager> ().ToSelf ().InSingletonScope();
      Bind<NetworkManager> ().ToSelf ().InSingletonScope();
      Bind<DataStore> ().ToSelf ().InSingletonScope();
    }
  }
}
