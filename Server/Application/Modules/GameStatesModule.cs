using System;
using Common.Game.Networking;
using Server.Game.Networking.MessageHandlers;

namespace Server
{
  public class GameStatesModule : Ninject.Modules.NinjectModule
  {
    public override void Load ()
    {
      Bind<GameStateManager> ().ToSelf ().InSingletonScope();
      Bind<IGameState> ().To<InititializeState> ();
      Bind<IGameState> ().To<LobbyState> ();
    }
  }
}

