using System;
using DataLoader.Data;
using Common.Game.Data;
using Common;

namespace Server
{
  public class GameDataModule: Ninject.Modules.NinjectModule
  {
    public override void Load ()
    {
      Bind<BaseData> ().To<AbilityData> ();
      Bind<BaseData> ().To<MapData> ();
      Bind<BaseData> ().To<CharacterClassData> ();
      Bind<BaseData> ().To<WeaponData> ();
    }
  }
}

