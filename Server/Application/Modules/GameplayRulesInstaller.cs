using System.Collections.Generic;

namespace Server
{
  public class GameplayRulesInstaller
  {
    public static List<string> Scripts {get; private set;}

    static GameplayRulesInstaller ()
    {
      Scripts = new List<string> ();
      Scripts.Add ("TestGamePlayRule.py");
      Scripts.Add ("AbilityActivationRule.py");
      Scripts.Add ("CombatRule.py");
      Scripts.Add ("InputHandlingRule.py");
    }
  }
}

