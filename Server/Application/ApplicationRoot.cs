﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using Common.Game;
using Server.Application.Modules;
using System.Threading;
using System.Reflection;
using Common.Messaging;
using Game.Server.Services;
using Lidgren.Network;
using Common.Utility;
using Server.GameConsole;
using log4net;
using log4net.Config;
using Common;
using Common.Applications.Modules;

namespace Server.Application
{

  public class ApplicationRoot
  {
    private IGame game;
    private ConsoleCommand consoleCommand;
    private HiResTimer timer;
    private long currentTime;
    private long fixedUpdateAccumulator;
    private long networkUpdateAccumulator;

    private ILog log;

    public ApplicationRoot ()
    {
      timer = new HiResTimer ();
    }

    public void Main ()
    {
      var kernel = new StandardKernel (new NinjectSettings (), 
                                       new ServerInstaller (), 
                                       new NetworkMessageInstaller(),
                                       new GameEntityFactoryModule(),
                                       new GameStatesModule(),
                                       new GameDataModule());
      game = kernel.Get<IGame> ();
      game.Start ();
      log = kernel.Get<log4net.ILog> ();
      consoleCommand = new ConsoleCommand (game, log);
      timer.Start ();
      Thread gameThread = new Thread (GameLoop) { IsBackground = true };
      gameThread.Start ();

      while (game.IsRunning)
      {
        HandleInput ();
      }
    }

    void GameLoop ()
    {
      while (game.IsRunning)
      {
        long time = timer.ElapsedMilliseconds;
        long dt = time - currentTime;

        fixedUpdateAccumulator += dt;
        networkUpdateAccumulator += dt;
       
        currentTime = timer.ElapsedMilliseconds;
        double dts = dt / 1000.0;
        game.Update (dts);
        if (fixedUpdateAccumulator >= TimeSettings.FixedUpdateRate)
        {
          game.FixedUpdate ();
          fixedUpdateAccumulator = 0;
        }

        if (networkUpdateAccumulator >= TimeSettings.NetworkUpdateRate)
        {
          game.NetworkUpdate ();
          networkUpdateAccumulator = 0;
        }
      }
    }

    void HandleInput ()
    {
      string input = Console.ReadLine ();
      string[] parameters = input.Split (' ');
      MethodInfo method = consoleCommand.GetType ().GetMethod (parameters [0]);

      if (method != null)
      {
        var parametersInfo = method.GetParameters ();
        if (parametersInfo.Length != parameters.Length - 1)
        {
          log.Error ("Incorrect number of arguments for command: \" + parameters [0]");
        } else
        {
          object[] convertedParams = new object[parameters.Length - 1];
          for (int i = 1; i < parameters.Length; i++)
          {
            convertedParams [i - 1] = Convert.ChangeType (parameters [i], parametersInfo [i - 1].ParameterType);
          }

          try
          {
            method.Invoke (consoleCommand, convertedParams);
            log.Debug("Success");
          } catch (Exception e)
          {
            log.Error (e);
          }
        }
      } else
      {
          log.Error("Command: " + parameters [0] + " does not exist");
      }
    }
  }
}
