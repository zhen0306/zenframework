using System;
using Common.Game;
using Common.Messaging;
using log4net;
using Server.Game;

namespace Server.GameConsole
{
  public class ConsoleCommand
  {
    IGame game;
    ILog log;

    public ConsoleCommand (IGame game, ILog log)
    {
      this.game = game;
      this.log = log;

      game.MessageBusManager.AddEventHandler<GameEvents.ConsolePrint> (PrintConsole);
    }

    private void PrintConsole(MessageBus<GameEvents.ConsolePrint> message)
    {
      log.Info (message.data.log);
    }

    public string LoadAll()
    {
      game.MessageBusManager.SendMessage<GameEvents.LoadAll> (new GameEvents.LoadAll ());
      return "Success";
    }

    public string LoadScript(string scriptName)
    {
      game.MessageBusManager.SendMessage<GameEvents.LoadScript> (new GameEvents.LoadScript (scriptName));
      return "Success";
    }

    public string ReloadScript(string scriptName)
    {
      game.MessageBusManager.SendMessage<GameEvents.ReloadScript> (new GameEvents.ReloadScript (scriptName));
      return "Success";
    }

    public string ActivateAbility(int id, string ability)
    {
      game.MessageBusManager.SendMessage<GameEvents.ActivateAbility> (new GameEvents.ActivateAbility (
        game.GameEntityManager.Entities [id],
        ability));
      return "Success";
    }

    public string ListEntities()
    {
      for (int i = 0; i < game.GameEntityManager.Entities.Count; i++)
      {
        log.Debug ("Entity " + i + " Type: " + game.GameEntityManager.Entities [i].GetType ());
      }
      return "Success";
    }

    public string DamageTarget(int id, int amount)
    {
      game.MessageBusManager.SendMessage (new GameEvents.DamageEvent (
        game.GameEntityManager.Entities [id],
        game.GameEntityManager.Entities [id],
        amount));

      return "Success";
    }

    public string DamageTargetFromEntity(int attacker, int target, int amount)
    {
      game.MessageBusManager.SendMessage (new GameEvents.DamageEvent (
        game.GameEntityManager.Entities [attacker],
        game.GameEntityManager.Entities [target],
        amount));

      return "Success";
    }
  }
}

