using System;
using log4net;

namespace Server
{
  public class CommonLogToLog4Net: Common.Log.ILog
  {
    private ILog log;

    public CommonLogToLog4Net (ILog log)
    {
      this.log = log;
    }

    #region ILog implementation

    public void Debug (object message)
    {
      log.Debug (message);
    }

    public void Info (object message)
    {
      log.Info (message);
    }

    public void Warn (object message)
    {
      log.Warn (message);
    }

    public void Error (object message)
    {
      log.Error (message);
    }

    #endregion
  }
}

