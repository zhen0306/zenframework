using System;
using System.Collections.Generic;
using System.Linq;
using GameEvents;
using Common.Messaging;
using Common.Game;

namespace Server
{
  public class GameStateManager
  {
    public Dictionary<GameplayStates, IGameState> map;

    public IGameState CurrentState { get; private set; }

    private IGame game;

    public GameStateManager (IGame game, IEnumerable<IGameState> gameStates)
    {
      map = gameStates.ToDictionary (x => x.Type);
      this.game = game;

      game.MessageBusManager.AddEventHandler<GameStateTransition> (GameStateTransitionHandler);
    }

    private void GameStateTransitionHandler(MessageBus<GameStateTransition> message)
    {
      if (CurrentState != null)
      {
        CurrentState.End ();
      }
      CurrentState = map[message.data.State];
      CurrentState.Start ();
      game.MessageBusManager.SendMessage (new GameStateChanged (message.data.State));
    }
  }
}

