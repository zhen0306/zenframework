using System;
using System.Collections.Generic;
using Common.Game;
using Common.Messaging;
using Lidgren.Network;

namespace Server.Game.Networking
{
  public class NetworkManager
  {
    public Dictionary<long, GameClient> Clients { get; private set; }

    private IGame game;
    public NetworkManager (IGame game)
    {
      this.game = game;

      Clients = new Dictionary<long, GameClient> ();
      game.MessageBusManager.AddEventHandler<NetworkEvents.OnClientConnect> (ClientConnected);
      game.MessageBusManager.AddEventHandler<NetworkEvents.OnClientDisconnect> (ClientDisconnected);
    }

    private void ClientConnected(MessageBus<NetworkEvents.OnClientConnect> message)
    {
      Clients.Add (message.data.Connection.RemoteUniqueIdentifier, new GameClient (game, message.data.Connection));

      //temp
      game.MessageBusManager.SendMessage (new GameEvents.PlayerRequestSpawnEvent (Clients [message.data.Connection.RemoteUniqueIdentifier]));
    }

    private void ClientDisconnected(MessageBus<NetworkEvents.OnClientDisconnect> message)
    {
      long id = message.data.Id;
      Clients.Remove (id);
    }

    public void SendSnapshot(NetServer server)
    {
      foreach (GameClient client in Clients.Values)
      {
        client.SendSnapshot (server);
      }
    }
  }
}

