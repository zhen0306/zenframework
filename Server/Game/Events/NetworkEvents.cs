using System;
using Lidgren.Network;

namespace NetworkEvents
{
  public class OnClientConnect: EventArgs
  {
    public NetConnection Connection { get; private set; }
    public int AccountId { get; private set; }

    public OnClientConnect(NetConnection connection)
    {
      Connection = connection;
    }
  }

  public class OnClientAuthorized: EventArgs
  {
  }

  public class OnClientDisconnect: EventArgs
  {
    public long Id { get; private set; }

    public OnClientDisconnect(long id)
    {
      Id = id;
    }
  }


}

