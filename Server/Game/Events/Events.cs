﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Game;
using Server.Game.Networking;
using Server;

namespace GameEvents
{
  public class GameStateTransition : EventArgs
  {
    public GameplayStates State { get; private set; }

    public GameStateTransition(GameplayStates state)
    {
      State = state;
    }
  }

  public class GameStateChanged : EventArgs
  {
    public GameplayStates State { get; private set; }

    public GameStateChanged(GameplayStates state)
    {
      State = state;
    }
  }

  public class LoadAll : EventArgs
  {
  }

  public class LoadScript: EventArgs
  {
    public string ScriptName { get; private set; }

    public LoadScript (string scriptName)
    {
      ScriptName = scriptName;
    }
  }

  public class ConsolePrint: EventArgs
  {
    public string log { get; private set; }

    public ConsolePrint (string log)
    {
      this.log = log;
    }
  }

  public class ReloadScript: EventArgs
  {
    public string ScriptName { get; private set; }

    public ReloadScript (string scriptName)
    {
      ScriptName = scriptName;
    }
  }

  public class PlayerRequestSpawnEvent:EventArgs
  {
    public GameClient Client { get; private set; }

    public PlayerRequestSpawnEvent(GameClient client)
    {
      Client = client;
    }
  }

  public class PlayerSpawnEvent:EventArgs
  {
    public GameClient Client { get; private set; }
    public GameEntity GameEntity { get; private set; }

    public PlayerSpawnEvent(GameClient client, GameEntity entity)
    {
      Client = client;
      GameEntity = entity;
    }
  }

  public class SpawnEvent:EventArgs
  {
    public GameEntity GameEntity { get; private set; }

    public SpawnEvent(GameEntity entity)
    {
      GameEntity = entity;
    }
  }

  public class ActivateAbility:EventArgs
  {
    public GameEntity GameEntity { get; private set; }

    public string GUID { get; private set; }

    public ActivateAbility (GameEntity entity, string guid)
    {
      GameEntity = entity;
      GUID = guid;
    }
  }

  public class EndAbility:EventArgs
  {
    public GameEntity GameEntity { get; private set; }

    public string GUID { get; private set; }

    public EndAbility (GameEntity entity, string guid)
    {
      GameEntity = entity;
      GUID = guid;
    }
  }

  public class DamageEvent:EventArgs
  {
    public GameEntity Source { get; private set; }

    public GameEntity Target { get; private set; }

    public int Amount { get; private set; }

    public DamageEvent (GameEntity source, GameEntity target, int amount)
    {
      Source = source;
      Target = target;
      Amount = amount;
    }
  }

  public class HealEvent:EventArgs
  {
    public GameEntity Source { get; private set; }

    public GameEntity Target { get; private set; }

    public float Amount { get; private set; }

    public HealEvent (GameEntity source, GameEntity target, float amount)
    {
      Source = source;
      Target = target;
      Amount = amount;
    }
  }

  public class DeathEvent: EventArgs
  {
    public GameEntity Killer { get; private set; }

    public GameEntity Target { get; private set; }
    //public GameEntity Weapon?
    //public GameEntity Ability?
  }
}
