using System;
using Common.Game;
using Server.Game.Components;
using log4net;
using Server.Game.Networking;
using Lidgren.Network;

namespace Server.Game.Entities
{
  public class PlayerEntity : GameEntity
  {
    public override EntityTypes Type {
      get {
        return EntityTypes.Player;
      }
    }

    public AttributesComponent Attributes { get; private set; }
    public PlayerNetworkComponent Network { get; private set; }

    public GameClient Client { get; private set; }

    public PlayerEntity (GameClient client, ILog log)
    {
      log.Info ("created player entity");
      Attributes = AddComponent(new AttributesComponent ());
      Network = AddComponent (new PlayerNetworkComponent (client, this));
      Attributes.LoadData (100, 100);
      Client = client;
    }

    public class PlayerNetworkComponent : NetworkComponent
    {
      private PlayerEntity entity;

      public PlayerNetworkComponent(GameClient client, PlayerEntity entity) : base(client)
      {
        this.entity = entity;
      }

      public override void Serialize (Lidgren.Network.NetOutgoingMessage message)
      {
        message.Write (NetworkId);
        message.Write ((byte)entity.Type);
        message.Write (entity.Attributes.Health);
        message.Write (entity.Attributes.MaxHealth);
      }
    }
  }
}

