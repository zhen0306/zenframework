using System;
using Common.Game.Networking;
using Lidgren.Network;
using Common.Game.Networking.Messages;
using Server.Game;
using Common.Game;
using log4net;

namespace Server
{
  public class ConnectMessageHandler : INetworkMessageHandler
  {
    public byte Code {
      get {
        return (byte)MessageTypes.Connect;
      }
    }

    private ServerGame game;
    private ILog log;

    public ConnectMessageHandler (IGame game, ILog log)
    {
      this.game = game as ServerGame;
      this.log = log;
    }

    #region INetworkMessageHandler implementation

    public void Handler (NetIncomingMessage message)
    {
      game.MessageBusManager.SendMessage (new NetworkEvents.OnClientConnect (message.SenderConnection));
      log.Info (NetUtility.ToHexString (message.SenderConnection.RemoteUniqueIdentifier) + " connected!");
    }
    #endregion
  }
}

