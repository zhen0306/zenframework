using System;
using Common.Game.Networking;
using Lidgren.Network;
using Common.Game.Networking.Messages;

namespace Server
{
  public class ConnectAuthorizeHandler : INetworkMessageHandler
  {
    public byte Code {
      get {
        return (byte)MessageTypes.Authorize;
      }
    }

    public ConnectAuthorizeHandler ()
    {
    }

    #region INetworkMessageHandler implementation

    public void Handler (NetIncomingMessage message)
    {

    }
    #endregion
  }
}

