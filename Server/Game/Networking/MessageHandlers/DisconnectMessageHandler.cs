using System;
using Common.Game.Networking;
using Lidgren.Network;
using Common.Game.Networking.Messages;
using Server.Game;
using Common.Game;
using log4net;

namespace Server
{
  public class DisconnectMessageHandler : INetworkMessageHandler
  {
    public byte Code {
      get {
        return (byte)MessageTypes.Disconnect;
      }
    }

    private ServerGame game;
    private ILog log;

    public DisconnectMessageHandler (IGame game, ILog log)
    {
      this.game = game as ServerGame;
      this.log = log;
    }

    #region INetworkMessageHandler implementation

    public void Handler (NetIncomingMessage message)
    {
      game.MessageBusManager.SendMessage (new NetworkEvents.OnClientDisconnect (message.SenderConnection.RemoteUniqueIdentifier));
      log.Info (NetUtility.ToHexString (message.SenderConnection.RemoteUniqueIdentifier) + " disconnected!");
    }
    #endregion
  }
}

