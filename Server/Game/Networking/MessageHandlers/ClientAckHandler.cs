using System;
using Common.Game.Networking;
using Lidgren.Network;
using Common.Game.Networking.Messages;
using Server.Game;
using Common.Game;

namespace Server
{
  public class ClientAckHandler : INetworkMessageHandler
  {
    public byte Code {
      get {
        return (byte)MessageTypes.Ack;
      }
    }

    ServerGame game;

    public ClientAckHandler (IGame game)
    {
      this.game = game as ServerGame;
    }

    #region INetworkMessageHandler implementation

    public void Handler (NetIncomingMessage message)
    {
      //TODO: send an event so that networkmanager is decoupled
      long clientId = message.SenderConnection.RemoteUniqueIdentifier;
      int ack = message.ReadInt32 ();
      game.NetworkManager.Clients [clientId].ack = ack;
    }
    #endregion
  }
}

