using System;
using Common.Game.Networking;
using Common.Game.Networking.Messages;
using Common.Game;
using Lidgren.Network;
using Common;
using Common.GameEvents;
using Server.Game;

namespace Server.Game.Networking.MessageHandlers
{
  public class InputNetworkMessageHandler : INetworkMessageHandler
  {
    public byte Code {
      get {
        return (byte)MessageTypes.Input;
      }
    }

    private ServerGame game;

    public InputNetworkMessageHandler (IGame game)
    {
      this.game = game as ServerGame;
    }

    public void Handler (NetIncomingMessage message)
    {
      int ack = message.ReadInt32 ();
      InputTypes input = (InputTypes)message.ReadInt32 ();
      float deltaTime = message.ReadFloat ();
      game.MessageBusManager.SendMessage<Common.GameEvents.InputEvent> (new InputEvent(ack, message.SenderConnection.RemoteUniqueIdentifier, input, deltaTime));
      game.NetworkManager.Clients [message.SenderConnection.RemoteUniqueIdentifier].ackInput = ack;
    }
  }
}

