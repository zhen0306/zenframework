using System;
using Common.Game;
using Lidgren.Network;
using Common.Game.Networking.Messages;
using Common.Networking;

namespace Server.Game.Networking
{
  public class GameClient
  {
    public enum ClientState
    {
      Connecting,
      Primed,
      InWorld,
      Dead
    }

    public long NetworkId { 
      get {
        return connection.RemoteUniqueIdentifier;
      } 
    }

    public GameEntity GameEntity { get; set; }

    public ClientState State { get; set; }

    public int ackInput;
    public long ack;
    private ServerGame game;
    private NetConnection connection;

    public GameClient (IGame game, NetConnection connection)
    {
      this.game = game as ServerGame;
      this.connection = connection;
    }

    public void SendSnapshot (NetServer server)
    {
      Snapshot snapshot = game.SnapshotRepository.Snapshots.ElementAt (0);
      if (snapshot != null)
      {
        var message = server.CreateMessage ();
        message.Write ((byte)MessageTypes.Snapshot);
        //start doing sync stuff here
        //TODO: use ack to get difference of last state and current state
        message.Write (snapshot.Id);
        message.Write (ackInput); //send input ack to do proper csp
        message.Write (snapshot.Entities.Count);
        for (int i = 0; i < snapshot.Entities.Count; i++)
        {
          //TODO: do serialization for snapshot after ack
          var Entity = snapshot.Entities [i];
          Entity.GetComponent<NetworkComponent> ().Serialize (message);
        }

        server.SendMessage (message, connection, NetDeliveryMethod.Unreliable);
      }
    }
  }
}

