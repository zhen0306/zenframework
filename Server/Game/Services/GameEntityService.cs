using System;
using Common.Game;
using Common.Services;
using log4net;
using Server.Game.Entities;
using Common.Messaging;
using Server.Game;

namespace Server
{
  public class GameEntityService : IService
  {
    private ServerGame game;
    private ILog log;

    public bool Enabled { get; set; }

    public GameEntityService (IGame game, ILog log)
    {
      log.Info ("Created Game Entity Service");
      this.game = game as ServerGame;
      this.log = log;
    }

    public void Start ()
    {
      game.MessageBusManager.AddEventHandler<GameEvents.PlayerRequestSpawnEvent> (SpawnRequestEventHandler);
    }

    public void End ()
    {
      game.MessageBusManager.RemoveEventHandler<GameEvents.PlayerRequestSpawnEvent> (SpawnRequestEventHandler);
    }

    private void SpawnRequestEventHandler(MessageBus<GameEvents.PlayerRequestSpawnEvent> message)
    {
      PlayerEntity entity = new PlayerEntity (message.data.Client, log);
      game.GameEntityManager.AddEntity (entity.Network.NetworkId, entity);
      game.MessageBusManager.SendMessage (new GameEvents.PlayerSpawnEvent (message.data.Client, entity));
      message.data.Client.GameEntity = entity;
    }
  }
}

