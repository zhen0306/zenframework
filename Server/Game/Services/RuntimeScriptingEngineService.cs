﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Services;
using Microsoft.Scripting.Hosting;
using System.Reflection;
using System.IO;
using Common.Messaging;
using Common.Game;
using Microsoft.CSharp.RuntimeBinder;
using log4net;

namespace Server.Game.Services
{
  public class RuntimeScriptingEngineService : IService, IUpdateable
  {
    private ScriptEngine scriptEngine;
    private Dictionary<string, dynamic> scriptScopeLookup;
    private IGame game;
    private ILog log;

    public bool Enabled { get; set; }

    private string ScriptPath {
      get { 
        return Assembly.GetEntryAssembly ().Location + "/../../../../Scripts/";
      }
    }

    public RuntimeScriptingEngineService (IGame game, ScriptEngine scriptingEngine, ILog log)
    {
      this.game = game;
      this.scriptEngine = scriptingEngine;
      this.scriptScopeLookup = new Dictionary<string, dynamic> ();
      log.Info ("Created runtime script engine");
      this.log = log;

      game.MessageBusManager.AddEventHandler<GameEvents.LoadAll> (ReloadAllScripts);
      game.MessageBusManager.AddEventHandler<GameEvents.LoadScript> (LoadScriptHandler);
    }

    public void Start ()
    {

      var assemblies = AppDomain.CurrentDomain.GetAssemblies ();
      foreach (Assembly assembly in assemblies)
      {
        scriptEngine.Runtime.LoadAssembly (assembly);
      }

      foreach (string script in GameplayRulesInstaller.Scripts)
      {
        LoadScriptHandler (new MessageBus<GameEvents.LoadScript> (new GameEvents.LoadScript (script)));
      }
    }

    public void Update (double deltaTime)
    {
      List<dynamic> scripts = scriptScopeLookup.Values.ToList ();
      for (int i = 0; i < scripts.Count; i++)
      {
        var script = scripts [i];
        try
        {
          script.Update (deltaTime);
        } catch (Exception e)
        {
          log.Error (e.ToString ());
        }
      }
    }

    public void End ()
    {
      List<dynamic> scripts = scriptScopeLookup.Values.ToList ();
      for (int i = 0; i < scripts.Count; i++)
      {
        var script = scripts [i];
        try
        {
          script.End ();
        } catch (Exception e)
        {
          log.Error (e.ToString ());
        }
      }    
    }

    private void ReloadAllScripts (MessageBus<GameEvents.LoadAll> message)
    {
      List<string> keys = scriptScopeLookup.Keys.ToList ();
      foreach (string key in keys)
      {
        string path = ScriptPath + key;
        path = Path.GetFullPath (path);
        scriptScopeLookup [key].End ();
        scriptScopeLookup [key] = scriptEngine.ExecuteFile (path);
        scriptScopeLookup [key].game = game;
        scriptScopeLookup [key].Start ();
        log.Info ("Reloaded Script: " + key);
      }
    }

    private void LoadScriptHandler (MessageBus<GameEvents.LoadScript> message)
    {
      try
      {
        string path = ScriptPath + message.data.ScriptName;
        path = Path.GetFullPath (path);

        if (!scriptScopeLookup.ContainsKey (message.data.ScriptName))
        {
          scriptScopeLookup.Add (message.data.ScriptName, scriptEngine.Runtime.ExecuteFile (path));
          scriptScopeLookup [message.data.ScriptName].game = game;
          log.Info ("Loaded Script: " + message.data.ScriptName);
        } else
        {
          scriptScopeLookup [message.data.ScriptName].End ();
          scriptScopeLookup [message.data.ScriptName] = scriptEngine.Runtime.ExecuteFile (path);
          scriptScopeLookup [message.data.ScriptName].game = game;
          log.Info ("Reloaded Script: " + message.data.ScriptName);
        }
      } catch (Exception e)
      {
        log.Error (e.ToString ());
      }
      try
      {
        scriptScopeLookup [message.data.ScriptName].Start ();
      } catch (Exception e)
      {
        log.Error (e.ToString ());
      }
    }
  }
}
