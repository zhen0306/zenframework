﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Services;
using DataLoader;
using Common.Game;
using DataLoader.Data;
using log4net;

namespace Game.Server.Services
{
  public class DataLoaderService : IService
  {
    private IGame game;
    private IDataLoader dataLoader;
    private List<BaseData> dataTypes;
    private ILog log;

    public DataLoaderService (IGame game, IDataLoader dataLoader, IEnumerable<BaseData> dataTypes, ILog log)
    {
      log.Info ("Created Data Loader Service");
      this.game = game;
      this.dataLoader = dataLoader;
      this.dataTypes = dataTypes.ToList ();
      this.log = log;
    }
    #region IService implementation
    public bool Enabled { get; set; }
    #endregion
    public void Start ()
    {
      log.Info ("Begin Loading Data");
      //begin loading data
      //data loader object needs to call load all
      //data loader implementation will handle the rest
      for (int i = 0; i < dataTypes.Count; i++)
      {
        dataLoader.LoadAll (dataTypes [i].GetType ());
      }
      log.Info ("Completed Loading Data");
    }

    public void End ()
    {
    }
  }
}
