﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Services;
using Common.Game;
using Lidgren.Network;
using log4net;
using Server.Game.Networking;
using Common.Game.Networking;
using Common.Game.Networking.Messages;

namespace Server.Game.Services
{
  public class NetworkService: IService, INetworkUpdateable
  {
    private NetServer server;
    private ServerGame game;
    private ILog log;
    private Dictionary<byte, INetworkMessageHandler> handlers;

    public bool Enabled { get; set; }

    public NetworkService (IGame game, NetServer server, ILog log, IEnumerable<INetworkMessageHandler> handlers)
    {
      log.Info("Created NetworkService");

      this.game = game as ServerGame;
      this.server = server;
      this.log = log;
      this.handlers = handlers.ToDictionary<INetworkMessageHandler,byte> ((message) => message.Code);
    }

    public void Start ()
    {
      server.Start (); 
    }

    public void NetworkUpdate ()
    {
      NetIncomingMessage msg;
      while ((msg = server.ReadMessage()) != null)
      {
        switch (msg.MessageType)
        {
        case NetIncomingMessageType.DiscoveryRequest:
          //
          // Server received a discovery request from a client; send a discovery response (with no extra data attached)
          //
          server.SendDiscoveryResponse (null, msg.SenderEndPoint);
          log.Info ("Discovered");
          break;
        case NetIncomingMessageType.VerboseDebugMessage:
        case NetIncomingMessageType.DebugMessage:
          log.Info(msg.ReadString ());
          break;
        case NetIncomingMessageType.WarningMessage:
          log.Warn(msg.ReadString ());
          break;
        case NetIncomingMessageType.ErrorMessage:
           log.Error(msg.ReadString ());
          break;
        case NetIncomingMessageType.StatusChanged:
          NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte ();
          log.Info (status);
          if (status == NetConnectionStatus.Connected)
          {
            handlers [(byte)MessageTypes.Connect].Handler (msg);
          } else if (status == NetConnectionStatus.Disconnected)
          {
            handlers [(byte)MessageTypes.Disconnect].Handler (msg);
          }
          break;
        case NetIncomingMessageType.Data:
          byte code = msg.ReadByte ();
          handlers [code].Handler (msg);

          break;  
        default:
          log.Warn ("Unhandled type: " + msg.MessageType);
          break;
        }
        server.Recycle (msg);
      }

      //create snapshot
      game.SnapshotRepository.CreateSnapshot (game);
      //now write snapshots
      game.NetworkManager.SendSnapshot (server);
    }

    public void End ()
    {
            
    }
  }
}
