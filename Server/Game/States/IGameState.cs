using System;

namespace Server
{
  public interface IGameState
  {
    GameplayStates Type { get; }
    void Start();
    void End();
  }
}

