using System;

namespace Server
{
  public enum GameplayStates
  {
    Initialize,
    Lobby,
    Loading,
    Game,
    End
  }
}

