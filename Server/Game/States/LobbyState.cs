using System;
using Common.Game;
using log4net;

namespace Server
{
  public class LobbyState : IGameState
  {
    private IGame game;
    private ILog log;

    public LobbyState (IGame game, ILog log)
    {
      this.game = game;
      this.log = log;
    }

    #region IGameState implementation

    public void Start ()
    {
      //start lobby services
      log.Debug ("Starting Lobby");
    }

    public void End ()
    {
      //end lobby service
    }

    public GameplayStates Type {
      get {
        return GameplayStates.Lobby;
      }
    }

    #endregion
  }
}

