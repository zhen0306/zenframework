using System;
using Common.Game;

namespace Server
{
  public class GameplayRunningState : IGameState
  {
    private IGame game;

    public GameplayRunningState (IGame game)
    {
      this.game = game;
    }

    #region IGameState implementation

    public void Start ()
    {

    }

    public void End ()
    {

    }

    public GameplayStates Type {
      get {
        return GameplayStates.Game;
      }
    }

    #endregion
  }
}

