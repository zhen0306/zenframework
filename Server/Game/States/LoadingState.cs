using System;

namespace Server
{
  public class LoadingState : IGameState
  {
    public LoadingState ()
    {
    }

    #region IGameState implementation

    public void Start ()
    {
      throw new NotImplementedException ();
    }

    public void End ()
    {
      throw new NotImplementedException ();
    }

    public GameplayStates Type {
      get {
        return GameplayStates.Loading;
      }
    }

    #endregion
  }
}

