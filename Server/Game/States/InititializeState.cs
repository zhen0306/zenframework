using System;
using log4net;
using Server.Game;
using Common.Game;

namespace Server
{
  public class InititializeState : IGameState
  {
    private ServerGame game;
    private ILog log;

    public InititializeState (IGame game, ILog log)
    {
      this.log = log;
      this.game = game as ServerGame;
    }
    #region IGameState implementation
    public void Start ()
    {
      game.Services.ForEach ((s) => {
        s.Enabled = true; 
        s.Start ();
      });

      game.MessageBusManager.SendMessage (new GameEvents.GameStateTransition (GameplayStates.Lobby));
    }

    public void End ()
    {

    }

    public GameplayStates Type {
      get {
        return GameplayStates.Initialize;
      }
    }
    #endregion
  }
}

