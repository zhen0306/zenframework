using System;

namespace Server
{
  public class EndState : IGameState
  {
    public EndState ()
    {
    }

    #region IGameState implementation

    public void Start ()
    {
      throw new NotImplementedException ();
    }

    public void End ()
    {
      throw new NotImplementedException ();
    }

    public GameplayStates Type {
      get {
        return GameplayStates.End;
      }
    }

    #endregion
  }
}

