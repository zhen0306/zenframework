﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Services;
using Common.Game;
using Common.Messaging;
using Ninject;
using log4net;
using Server.Game.Networking;
using Common.Game.Managers;
using Common.Networking;
using GameEvents;
using Common.DataLoader;

namespace Server.Game
{
  public class ServerGame : IGame
  {
    public bool IsRunning {
      get {
        return true;
      }
    }

    [Inject]
    public GameStateManager GameStateManager { get; private set; }

    public MessageBusManager MessageBusManager { get; private set; }

    public GameEntityManager GameEntityManager { get; private set; }

    public DataStore DataStore { get; private set; }

    [Inject]
    public NetworkManager NetworkManager { get; private set; }

    [Inject]
    public SnapshotRepository SnapshotRepository { get; private set; }

    [Inject]
    public List<IService> Services { get; private set; }

    private Dictionary<Type, IService> ServiceMap;

    public List<IUpdateable> UpdatableService { get; private set; }

    public List<IFixedUpdateable> FixedUpdatableService { get; private set; }

    public List<INetworkUpdateable> NetworkUpdatableService { get; private set; }

    private ILog log;

    public ServerGame (MessageBusManager messageBusManager, GameEntityManager gameEntityManager, DataStore dataStore, ILog log)
    {
      GameEntityManager = gameEntityManager;
      MessageBusManager = messageBusManager;
      DataStore = dataStore;

      UpdatableService = new List<IUpdateable> ();
      FixedUpdatableService = new List<IFixedUpdateable> ();
      NetworkUpdatableService = new List<INetworkUpdateable> ();
      this.log = log;
    }

    public void Start ()
    {
      log.Info ("Starting Game");
      ServiceMap = Services.ToDictionary (x => x.GetType ());
      foreach (IService service in Services)
      {
        if (service is IUpdateable)
        {
          UpdatableService.Add (service as IUpdateable);
        }
        if (service is IFixedUpdateable)
        {
          FixedUpdatableService.Add (service as IFixedUpdateable);
        }
        if (service is INetworkUpdateable)
        {
          NetworkUpdatableService.Add (service as INetworkUpdateable);
        }
      }
      MessageBusManager.SendMessage (new GameStateTransition (GameplayStates.Initialize));
    }

    public void Update (double deltaTime)
    {
      for (int i = 0; i < UpdatableService.Count; i++)
      {
        if ((UpdatableService [i] as IService).Enabled)
          UpdatableService [i].Update (deltaTime);
      }
    }

    public void FixedUpdate ()
    {
      for (int i = 0; i < FixedUpdatableService.Count; i++)
      {
        if ((FixedUpdatableService [i] as IService).Enabled)
          FixedUpdatableService [i].FixedUpdate ();
      }
    }

    public void NetworkUpdate ()
    {
      for (int i = 0; i < NetworkUpdatableService.Count; i++)
      {
        
        if ((NetworkUpdatableService [i] as IService).Enabled)
          NetworkUpdatableService [i].NetworkUpdate ();
      }
    }

    public void End ()
    {
      for (int i = 0; i < Services.Count; i++)
      {
        Services [i].End ();
      }
    }

    public T GetService<T>() where T:IService
    {
      return (T)ServiceMap [typeof(T)];
    }
  }
}
