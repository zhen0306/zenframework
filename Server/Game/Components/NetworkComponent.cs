using System;
using Common.Game;
using Server.Game.Networking;
using Lidgren.Network;

namespace Server
{
  public abstract class NetworkComponent : IComponent
  {
    private static int networkIdCount = 0;

    public int NetworkId { get; private set; }
    public GameClient Client { get; private set; }

    //do not want people to call base constructor
    private NetworkComponent()
    {
    }

    public NetworkComponent (GameClient client)
    {
      Client = client;
      NetworkId = networkIdCount;
      networkIdCount++;
    }

    public abstract void Serialize (NetOutgoingMessage message);
  }
}

