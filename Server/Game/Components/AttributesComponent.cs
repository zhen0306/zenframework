using System;
using Common.Game;

namespace Server.Game.Components
{
  public class AttributesComponent : IComponent
  {
    public int Health { get; set; }
    public int MaxHealth { get; private set; }

    public void LoadData(int health, int maxHealth)
    {
      Health = health;
      MaxHealth = maxHealth;
    }
  }
}

