﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLoader;

namespace Server.DataLoader
{
    public class DataLoader : IDataLoader
    {
        public void LoadAll<T>()
        {
        }

        public void Load<T>(string path)
        {
        }

        public void Unload<T>(T data)
        {
        }

        public void UnloadAll<T>()
        {
        }
    }
}
