﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLoader;
using Common.DataLoader;
using DataLoader.Data;
using JsonFx;
using log4net;
using System.IO;
using JsonFx.Json;
using System.Reflection;

namespace Server.DataLoader
{
  public class FileDataLoader : IDataLoader
  {
    private string directoryPath = "unity directory";
    private DataStore dataStore;
    private ILog log;

    public FileDataLoader (DataStore dataStore, ILog log)
    {
      this.dataStore = dataStore;
      this.log = log;
      directoryPath = Assembly.GetExecutingAssembly ().Location + "../../../../../Unity/Assets/Data/";
      directoryPath = Path.GetFullPath (directoryPath);
      log.Debug (directoryPath);
    }

    public void LoadAll<T> ()
    {
      LoadAll (typeof(T));
    }

    public void LoadAll (Type type)
    {
      log.Info ("Loading all data of type " + type.Name);
      //load everything in directory
      string path = directoryPath + type.Name;
      if (!Directory.Exists (path))
        return;

      var files = Directory.GetFiles (path, "*.json");
      foreach (string file in files)
      {
        Load (type, file);
      }
    }

    public void Load<T> (string path)
    {
      Load (typeof(T), path);
    }

    public void Load (Type type, string path)
    {
      log.Info ("Loading file " + path);
      string fullPath = path;
      string text = File.ReadAllText (fullPath);
      var reader = new JsonReader ();
      BaseData data = reader.Read (text, type) as BaseData;
      dataStore.AddData (data);
    }

    public void Unload<T> (T data)
    {
    }

    public void UnloadAll<T> ()
    {
    }

    public void UnloadAll (Type type)
    {
    }

    public void Save<T> (T data)
    {
      throw new NotImplementedException ();
    }
  }
}
